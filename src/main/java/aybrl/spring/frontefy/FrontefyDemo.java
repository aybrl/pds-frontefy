package aybrl.spring.frontefy;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class FrontefyDemo {

	@GetMapping("/")
	String home() {
		return "Frontefy is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(FrontefyDemo.class, args);
	}
}